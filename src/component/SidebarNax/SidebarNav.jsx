import React from "react";
import "./SidebarNav.css";

const SidebarNav = ({ kategori, subKategori, onClick }) => {
	// panggil semua kategori
	return (
		<div className="p-3 bg-primary text-white justify-content-between flex-column">
			<ul className="list-unstyled ps-0">
				<li className="mb-1">
					<button
						className="btn btn-toggle align-items-center rounded collapsed"
						data-bs-toggle="collapse"
						data-bs-target={`#${kategori}-collapse`}
						aria-expanded="true"
					>
						{kategori}
					</button>
					<div
						className="collapse show"
						id={`#${kategori}-collapse`}
					>
						<ul className="btn-toggle-nav list-unstyled fw-normal pb-1 small">
							<a
								className="btn btn-success"
								href="#"
								// kirim data biar bisa muncul
								onClick={() => {
									onClick(subKategori);
								}}
							>
								{subKategori}
							</a>
						</ul>
					</div>
				</li>
			</ul>
		</div>
	);
};

export default SidebarNav;
