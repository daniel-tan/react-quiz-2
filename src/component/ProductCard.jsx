import React, { useState } from "react";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";

function ProductCard({ nama, hargaAwal, hargaDiskon, gambar, slug }) {
	// const [product, setProduct] = useState(null);

	return (
		<Card>
			<Card.Img variant="top" src={gambar} />
			<Card.Body>
				<Card.Title>{nama}</Card.Title>
				<Card.Text>
					<s>{hargaAwal}</s> <br />
					{hargaDiskon}
				</Card.Text>
				<Link to={`/product/${slug}`} className="btn btn-info">
					{" "}
					Detail Produk
				</Link>
			</Card.Body>
		</Card>
	);
}

export default ProductCard;
