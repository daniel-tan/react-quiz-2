import React, { useEffect, useState } from "react";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Image from "react-bootstrap/Image";
import { useParams } from "react-router-dom";
import axios from "axios";

import Navigation from "../component/Navigation";
const DetailProduct = () => {
	const { id } = useParams();
	const [product, setProduct] = useState({});
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		const getData = async () => {
			try {
				setLoading(false);
				const { data } = await axios.get(
					`https://api.lemonilo.com/v1/product/` + id
				);
				setProduct(data.data);
				setLoading(true);
			} catch (error) {
				console.log("error fetch data : ", error);
			}
		};
		getData();
	}, [id]);

	return (
		<>
			<Navigation />
			<Container className="p-4">
				<Row sm={1} md={2}>
					<Col>
						{loading ? (
							<Image
								fluid={true}
								src={
									product?.productImages[0][
										"photoUrl"
									]
								}
								alt={product?.name}
								width="100%"
								height="auto"
							></Image>
						) : (
							<div
								style={{ height: 200, width: 300 }}
								backgroundColor={"gray"}
							></div>
						)}
					</Col>
					<Col>
						<h2>{product?.name}</h2>
						<h4>{product?.netWeight * 1000} gr</h4>
						<br />
						<b>Harga : {product?.sellPrice}</b>
						<br />
						Deskripsi Produk:
						<div
							dangerouslySetInnerHTML={{
								__html: product?.description,
							}}
						></div>
					</Col>
				</Row>
			</Container>
		</>
	);
};

export default DetailProduct;
