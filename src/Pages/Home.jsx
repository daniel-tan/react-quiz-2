import React, { useEffect, useState } from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";

import "../styles/App.css";
import Navigation from "../component/Navigation";
import ProductCard from "../component/ProductCard";
import SidebarNav from "../component/SidebarNax/SidebarNav";

const Home = () => {
	const [product, setProduct] = useState([]);
	const [categories, setCategories] = useState([]);

	const getData = async () => {
		try {
			const { data } = await axios.get(
				"https://api.lemonilo.com/v1/product/popular?&#39"
			);
			setProduct(data.data);
			// console.log("cek data = ", product);
		} catch (error) {
			console.log("error fetch data : ", error);
		}
	};

	const getCategories = async () => {
		try {
			const { data } = await axios.get(
				"https://api.lemonilo.com/v1/product/popular?&#39"
			);
			// setCategories({...new Set(data)});
			let test = [
				...new Set(
					data.data.productSubCategory.productCategory.name
				),
			];
			// console.log(
			// 	"cek ambil data : ",
			// 	data.data[0].productSubCategory.name
			// );
			console.log("test es6 categories : ", test);
			// return data;
		} catch (error) {
			console.log("error getCategories : ", error);
		}
	};

	useEffect(() => {
		getData();
		// getCategories();
	}, []);

	return (
		<>
			<Navigation />
			<Container fluid className="p-3">
				<Row>
					<Col className="col-md-2">
						{product.map((kategori, index) => {
							return (
								<SidebarNav
									key={index}
									kategori={
										kategori.productSubCategory
											.productCategory.name
									}
									subKategori={
										kategori.productSubCategory
											.name
									}
								></SidebarNav>
							);
						})}
					</Col>
					<Col>
						<Row
							xs={1}
							sm={3}
							md={5}
							lg={7}
							className="g-4 justify-content-center"
						>
							{product.map((produk, index) => {
								return (
									<Col key={index}>
										<ProductCard
											gambar={produk.photoUrl}
											hargaAwal={
												produk.gimmickPrice
											}
											hargaDiskon={
												produk.price
											}
											nama={produk.name}
											slug={produk.slug}
										></ProductCard>
									</Col>
								);
							})}
						</Row>
					</Col>
				</Row>
			</Container>
		</>
	);
};

export default Home;
