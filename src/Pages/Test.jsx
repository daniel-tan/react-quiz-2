// import { useParams } from "react-router-dom";

// const Test = () => {
// 	const { id } = useParams();
// 	return (
// 		<div>
// 			<h1>Coba berhasil!</h1>
// 			<br />
// 			<h3>Cek Data = {id}</h3>
// 		</div>
// 	);
// };

// export default Test;

import React, { useEffect, useState } from "react";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import { useParams } from "react-router-dom";
import axios from "axios";

import Navigation from "../component/Navigation";
const Test = () => {
	const { id } = useParams();
	const [product, setProduct] = useState();

	// let contoh = `https://api.lemonilo.com/v1/product/${id}`;
	// console.log("get params : ", contoh);

	const getDetail = () => {
		try {
			// const { data } = axios.get(
			// 	`https://api.lemonilo.com/v1/product/${id}`
			// );
			// setProduct(data.data.data);

			axios.get(`https://api.lemonilo.com/v1/product/${id}`)
				.then((res) => {
					const { data } = res.data;
					setProduct(data);
					console.log("get detail : ", product);
				})
				.catch((err) => {
					console.log("axios error: " + err);
				});
		} catch (error) {
			console.log("error fetch data : ", error);
		}
	};

	// useEffect(() => {
	// 	getDetail();
	// });

	getDetail();

	return (
		<>
			<Navigation />
			<Container>
				<Row sm={1} md={2}>
					<Col>
						<img
							src={product.photoUrl}
							alt={product.name}
							width="100%"
							height="200px"
						></img>
					</Col>
					<Col>
						{product.name}
						<br />
						{product.netWeight * 1000} gr
						<br />
						<s>{product.gimmickPrice}</s> {product.price}
						<br />
						Deskripsi Produk:
						{product.metaTitle.metaDescription}
					</Col>
				</Row>
			</Container>
		</>
	);
};

export default Test;
