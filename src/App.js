import React from "react";
import { Routes, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import "./styles/App.css";
import Home from "./Pages/Home";
import DetailProduct from "./Pages/DetailProduct";
import Test from "./Pages/Test";

const App = () => {
	return (
		<>
			<Routes>
				<Route path="/" element={<Home />} />
				<Route path="product/:id" element={<DetailProduct />} />
				{/* <Route path="*" element={<Test />} /> */}
			</Routes>
		</>
	);
};

export default App;
